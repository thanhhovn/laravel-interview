<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	   DB::table('posts')->insert(array(
		 array(
		    'description' => 'Adding multiple data in seeder 1',
		    'title' => 'Laravel Post1',
		    'is_active' => 1,
		 ),
		 array(
		    'description' => 'Adding multiple data in seeder 2',
		    'title' => 'Laravel Post2',
		    'is_active' => 1,
		 ),
		 array(
		    'description' => 'Adding multiple data in seeder 3',
		    'title' => 'Laravel Post3',
			'is_active' => 1,
		 ),		 
		 array(
		    'description' => 'Adding multiple data in seeder 4',
		    'title' => 'Laravel Post4',
			'is_active' => 1,
		 ),		 
		 array(
		    'description' => 'Adding multiple data in seeder 5',
		    'title' => 'Laravel Post5',
			'is_active' => 1,
		 ),		 
		 array(
		    'description' => 'Adding multiple data in seeder 6',
		    'title' => 'Laravel Post6',
			'is_active' => 1,
		 ),		 
	   ));
    }
}
