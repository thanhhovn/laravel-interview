<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/profile', 'TodoController@profile');
Route::get('/posts/edit/{postid}', 'PostController@edit');
Route::get('/posts/delpost/{postid}', 'PostController@delpost');
Route::get('/posts/delselectedpost/{poststr}', 'PostController@delselectedpost');
Route::get('/posts/genpdf/{postid}', 'PostController@genpdf');
Route::get('/posts/sendemail/{postid}', 'PostController@sendmail');
Route::post('/posts/editpost/{postid}', 'PostController@editpost');
Route::post('/posts/sendmailattachment/{postid}', 'PostController@sendmailattachment');
Auth::routes();

Route::get('/home', 'HomeController@index');
