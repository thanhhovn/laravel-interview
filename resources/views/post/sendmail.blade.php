@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
		<div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
					<form action="/posts/sendmailattachment/{{ $postid}}" method="POST">
						{{ csrf_field() }}
						<div class="row">
							<label for="emailaddress">Address </label>
							<input type="text" value="" name="emailaddress">
						</div>
						<div class="row">
							<label for="content">Email Content </label>
							<textarea name="emailcontent"></textarea>
						</div>
						<input type="submit" value="Send"</input>
					</form>                   
                </div>
            </div>
        </div>
		
	</div>
</div>
@endsection