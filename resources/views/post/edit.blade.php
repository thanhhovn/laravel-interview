@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
		<form action="/posts/editpost/{{ $postidtoedit->id}}" method="POST">
			{{ csrf_field() }}
			<div class="row">
				<label for="title">Title </label>
				<input type="text" value="{{$postidtoedit->title}}" name="title">
			</div>
			<div class="row">
				<label for="descript">Description </label>
				<textarea name="description">{{$postidtoedit->description}}</textarea>
			</div>
			<input type="submit" value="Save"</input>
		</form>
	</div>
</div>
@endsection