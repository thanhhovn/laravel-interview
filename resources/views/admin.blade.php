@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="row">
						<div class="col-md-12" style="text-align: center;">
							<input type="checkbox" id="checkall" name="checkall">
							<label for="checkall">Check All Posts</label>
							<input type="button" value="Remove" id="removeall" name="removeall"/>
						</div>
					</div>
					<div class="row" id="postpro">
					@foreach($allpost as $perpost)
						<div class="row">
							<div class="col-md-1 col-sm-1">
								<input type="checkbox" id="{{ $perpost->id }}" name="{{ $perpost->id }}" class="checktoremove">
							</div> 								
							<div class="col-md-2 col-sm-2">
								<div id="posttitle">
									{{ $perpost->title }}
								</div>
							</div>							
							<div class="col-md-3 col-sm-3">
								<div id="postdescription">
									{{ $perpost->description }}
								</div>
							</div>
							<div class="col-md-1 col-sm-1">
								<a href="/posts/edit/{{ $perpost->id  }}">Edit</a>
							</div> 
							<div class="col-md-1 col-sm-1">
								<a href="/posts/delpost/{{ $perpost->id  }}">Remove</a>
							</div> 
							<div class="col-md-2 col-sm-2">
								<a href="/posts/genpdf/{{ $perpost->id  }}">Generate Pdf</a>
							</div> 
							<div class="col-md-2 col-sm-2">
								<a href="/posts/sendemail/{{ $perpost->id  }}">Send Email</a>
							</div>
						</div> 
					@endforeach					
					</div> 
					<div class="row">
						<div class="col-md-6">
							{{ $allpost->fragment('title')->links("pagination::bootstrap-4") }}
						</div>
					</div>					
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$( document ).ready(function() {
		$( "#removeall" ).click(function() {
				var postidtodel = "";
			  $('.checktoremove').each(function () {
				   var sThisVal = (this.checked ? $(this).val() : "");
				   //console.log($(this).attr("id"));
				   postidtodel = postidtodel + $(this).attr("id") + ",";
			  });
			  var finalpoststr = postidtodel.substring(0,postidtodel.length - 1);
			  var deleteurl = "/posts/delselectedpost/" + finalpoststr;
			$.ajax({
			  url: deleteurl,  
			  success: function(data) {
				 if(data == "Sucess"){
					 location.reload(); 
				 }else{
					 alert("delete fail");
				 }
			  }
		   });			  
		});
		
		$( "#checkall" ).click(function() {
		  console.log($( ".checktoremove" ).is(":checked"));
		  if($( ".checktoremove" ).is(":checked")){
			  $('.checktoremove').prop('checked', false);
		  }else{
			  $('.checktoremove').prop('checked', true);
			  //console.log($('.checktoremove').attr("id"));
			 		  
			  
		  }
		});
	});	
</script>
@endsection
