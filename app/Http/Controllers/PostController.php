<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use Auth;
use PDF;
class PostController extends Controller
{
    public function edit(Request $request)
    {
		if(!Auth::user()->hasRole('admin')){
			return redirect()->to('/home');
		}
        $postid = $request->postid;
		//dd($postid);
		$postidtoedit = Posts::find($postid);
		return view('post.edit', ['postidtoedit' => $postidtoedit]);
    }
	
	public function editpost(Request $request)
    {
		if(!Auth::user()->hasRole('admin')){
			return redirect()->to('/home');
		}
        $postid = $request->postid;
		$postidtosave = Posts::find($postid);
		$postidtosave->title = $request->title;
		$postidtosave->description = $request->description;
		$postidtosave->save();
		return redirect('/home')->with('status', 'Post updated!');;   
		//dd($todotosave);
    }
	
    public function delpost(Request $request)
    {
		if(!Auth::user()->hasRole('admin')){
			return redirect()->to('/home');
		}
        $postid = $request->postid;
		$postidtodel = Posts::find($postid);
		$postidtodel->delete();
		return redirect()->to('/home');
    }	
	
    public function delselectedpost(Request $request)
    {
		if(!Auth::user()->hasRole('admin')){
			return redirect()->to('/home');
		}
		$poststr = $request->poststr;
		
		
		$postids = explode(",", $poststr);
		//dd($postids);
		// call delete on the query builder (no get())
		if(Posts::whereIn('id', $postids)->delete()){
			return "Sucess";	
		}else{
			return "Fail";
		} 
    }

	public function genpdf(Request $request)
    {
		if(!Auth::user()->hasRole('admin')){
			return redirect()->to('/home');
		}
        $postid = $request->postid;
		$postidtogen = Posts::find($postid);

		$data = [
		'postidtogen' => $postidtogen,
		];
		$pdf = PDF::loadView('pdf.document', $data);
		$postpdftosave = "pdf/" . $postid . ".pdf";
		$pdf->save($postpdftosave);
		return "Success to Generate Pdf";
	}

	public function sendmail(Request $request)
    {
		if(!Auth::user()->hasRole('admin')){
			return redirect()->to('/home');
		}
        $postid = $request->postid;
		return view('post.sendmail', ['postid' => $postid]); 
	}
	
	
    public function sendmailattachment(Request $request)
    {
		if(!Auth::user()->hasRole('admin')){
			return redirect()->to('/home');
		}
        $postid = $request->postid;
		$emailcontent = $request->emailcontent;
		$content = [
    		
    		"Content" => $emailcontent			
		];
		$emails = $request->emailaddress;
    	$subjects = "Larave sendmail";
    	$to = $emails;
        // here we add attachment, attachment must be an array
		$fileattachment = "pdf/" . $postid . ".pdf";
         $finalfilepath = public_path($fileattachment);
    	$data = array('name'=>"Thanh Ho");
    	 Mail::send(['text'=>'mail'], $data, function($message) {
         $message->to('abc@gmail.com', 'Larave sendmail')->subject
            ($emailcontent);
		$message->attach($finalfilepath);	
         $message->from('thanhhovn2016@gmail.com','Thanhho');
      });
    }	
	
}
