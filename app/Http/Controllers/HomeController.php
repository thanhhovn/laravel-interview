<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Posts;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if(Auth::user()->hasRole('admin')){
			$allpost = Posts::paginate(5);
			return view('admin',['allpost' => $allpost]);
		}

		if(Auth::user()->hasRole('user')){
			return view('home');
		}
        
    }
}
