<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use jeremykenedy\LaravelRoles\Models\Role;
class TodoController extends Controller
{
     public function userlogin()
    {
		return view('blog.userlogin');
    }
	
	 public function checkuserlogin(Request $request)
    {
		if(!Auth::attempt(['email' => $request->email, 'password' => $request->password]))
		{
			return redirect('/')->with('alert','Wrong password.');
		}
        return redirect()->to('/profile');
		
    }
	
	
	
		public function profile()
    {
		//dd(Auth::user()->hasRole('admin'));
		//dd(Auth::user()->hasPermission('create.users'));
		if (Role::where('name', '=', 'Smod')->first() === null) {
			$modrole = config('roles.models.role')::create([
				'name' => 'Smod',
				'slug' => 'smod',
				'description' => '',
				'level' => 5,
			]);
		}else{
			$modrole = Role::where('name', '=', 'Smod')->first();
		}
		//$user = config('roles.models.defaultUser')::find(Auth::id());
		$user = User::find(Auth::id());
		$user->attachRole($modrole);
		return view('blog.profile');
    }
}
